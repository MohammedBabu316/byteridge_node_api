﻿require('rootpath')();
const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const jwt = require('_helpers/jwt');
const errorHandler = require('_helpers/error-handler');
const userService = require('./users/user.service')

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

// use JWT auth to secure the api
app.use(jwt());

// api routes
app.use('/users', require('./users/users.controller'));
app.use('/audit', (req,res)=>{
    if(!req.user || !req.user.role || (req.user.role && req.user.role.toLowerCase() !== 'auditor')) {
        return res.status(401).json({ message: 'User Not Authorized' });
    }
    userService.getAudit()
        .then(users => {
            res.json(users)
        })
        .catch(err => {
            next(err)
        });
});

// global error handler
app.use(errorHandler);

// start server
const port = process.env.NODE_ENV === 'production' ? (process.env.PORT || 80) : 4000;
const server = app.listen(port, function () {
    console.log('Server listening on port ' + port);
});
